#tool "nuget:?package=NUnit.ConsoleRunner"
#tool "nuget:?package=JetBrains.dotCover.CommandLineTools"
#addin "nuget:?package=Cake.Hosts"

var target = Argument("target", "default");

var rootDir = Directory("./..");
var srcDir = rootDir + Directory("src");
var tmpDir = rootDir + Directory("tmp");
var deploymentDir = Directory("D:\\Deploy\\iTechArt-CakeBuild");

var windir = EnvironmentVariable("windir");
var domainName = "itechforum-cakebuild.com";
var ipAddress = "127.0.0.1";
var domainNameString = string.Format("       {0}        #Geterated by tool \"CakeBuild for CakeBuildExampleWebApp\"", domainName);

var siteName = "iTechForum-CakeBuild";
var siteUri = string.Format("http://{0}:80", domainName);
var physicalPath = deploymentDir.ToString();

Task("clean")
    .Does(() => {
        Information("Clean directories");
        if(!DirectoryExists(tmpDir)) {
            CreateDirectory(tmpDir);
        } else {
            CleanDirectories(tmpDir);
        }
    });

Task("build")
    .IsDependentOn("clean")
    .Does(() => {
        var configuration = Argument("configuration", "release");
        var slnFile = srcDir + File("CakeBuildExampleWebApp/CakeBuildExampleWebApp.sln");
        var tmpDirPath = MakeAbsolute(tmpDir).ToString();

        Information("Build solution \"{0}\" with configuration \"{1}\" to directory \"{2}\"", slnFile, configuration, tmpDirPath);     
        
        DotNetBuild(slnFile, settings => 
            settings
                .SetConfiguration(configuration)
                .WithProperty("OutputPath", tmpDirPath));            
    });

Task("test")
    .Does(() => {
        DotCoverCover(context => {
            context.NUnit3("./../tmp/*.Tests.dll");
        }, 
        new FilePath("./../tmp/_CodeCoverage/coverage.dcvr"),
        new DotCoverCoverSettings()
        .WithFilter("+:assembly=CakeBuildExampleWebApp;type=CakeBuildExampleWebApp.Controllers.*")
        .WithFilter("-:CakeBuildExampleWebApp.Tests"));       

        DotCoverReport(new FilePath("./../tmp/_CodeCoverage/coverage.dcvr"),
            new FilePath("./../tmp/_CodeCoverage/result.xml"),
            new DotCoverReportSettings {
                ReportType = DotCoverReportType.XML
            }); 

        var coveragePercentString = XmlPeek(new FilePath("./../tmp/_CodeCoverage/result.xml"), "/Root/@CoveragePercent");
        var coveragePercent = 0;
        int.TryParse(coveragePercentString, out coveragePercent);
        Information("CoveragePercent: {0}", coveragePercent);

        if(coveragePercent < 75) {
            throw new Exception("Too low coverage percent. Coverage percent should be at least 66.");
        }
    });

Task("create-website")
.Does(() => {
    if(!HostsRecordExists(ipAddress, domainNameString)) {
        AddHostsRecord(ipAddress, domainNameString);

        var arguments = new ProcessArgumentBuilder();
        arguments.Append("add");
        arguments.Append("sites");
        arguments.Append(string.Format("/name:\"{0}\"", siteName));
        arguments.Append(string.Format("/bindings:{0}", siteUri));
        arguments.Append(string.Format("/physicalPath:\"{0}\"", physicalPath));

        Information("windir: {0}", windir);
        var exitCodeWithArgument = StartProcess(windir + "/System32/inetsrv/appcmd", new ProcessSettings{ Arguments = arguments });
        // This should output 0 as valid arguments supplied
        Information("Exit code: {0}", exitCodeWithArgument);
    } else {
        Information("Stopping Site: {0}...", siteName);        
        var arguments = new ProcessArgumentBuilder();
        arguments.Append("stop");
        arguments.Append("site");
        arguments.Append(string.Format("/site.name:\"{0}\"", siteName));        
        var exitCodeWithArgument = StartProcess(windir + "/System32/inetsrv/appcmd", new ProcessSettings{ Arguments = arguments });
        // This should output 0 as valid arguments supplied
        Information("Exit code: {0}", exitCodeWithArgument);

        Information("Updating Site: {0}...", siteName);
        arguments = new ProcessArgumentBuilder();
        arguments.Append("set");
        arguments.Append("vdir");
        arguments.Append(string.Format("{0}/", siteName));
        arguments.Append(string.Format("-physicalPath:\"{0}\"", physicalPath));
        exitCodeWithArgument = StartProcess(windir + "/System32/inetsrv/appcmd", new ProcessSettings{ Arguments = arguments });
        // This should output 0 as valid arguments supplied
        Information("Exit code: {0}", exitCodeWithArgument);
        
        arguments = new ProcessArgumentBuilder();
        arguments.Append("set");
        arguments.Append("site");
        arguments.Append(siteName);
        arguments.Append(string.Format("/bindings:{0}", siteUri));
        exitCodeWithArgument = StartProcess(windir + "/System32/inetsrv/appcmd", new ProcessSettings{ Arguments = arguments });
        // This should output 0 as valid arguments supplied
        Information("Exit code: {0}", exitCodeWithArgument);
    }
});

Task("deploy")
    .IsDependentOn("create-website")
    .Does(() => {
        var publishDir = tmpDir + Directory("_PublishedWebsites") + Directory("CakeBuildExampleWebApp");
        CleanDirectories(deploymentDir);
        CopyDirectory(publishDir, deploymentDir);
    });

Task("run")
    .IsDependentOn("deploy")
    .Does(() => {
        var programFilesX86 = EnvironmentVariable("ProgramFiles(x86)");
        var arguments = new ProcessArgumentBuilder();
        arguments.Append(siteUri);
        StartProcess(programFilesX86 + "/Google/Chrome/Application/chrome", new ProcessSettings{ Arguments = arguments });
    });

Task("default")
    .IsDependentOn("build")
    .IsDependentOn("test")
    .IsDependentOn("deploy")
    .Does(() => {
        Information("Hello World!");
    });

RunTarget(target);